// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("jquery")
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

function checkValue() {
    var skill_field = $("#skill_skill");
    var skill_field_value = skill_field.val();

    if(skill_field_value.length >= 3) {
        $.get("/skill/slug_fetch", function(data, status){
            if(status === "success") {
                skill_field.val(data.slug);
                skill_field.prop("disabled", true)
            }
        });
    }
}

$(function(){
    $("#skill_skill").on("input", checkValue);
});

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
