Rails.application.routes.draw do
  get 'skill/index'
  get 'skill/slug_fetch'
  
  root 'skill#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
